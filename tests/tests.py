import argparse
import os
import unittest
from functools import reduce
from pathlib import Path
from tempfile import NamedTemporaryFile

from file import MB, ArgParser, Filter, TotalSize


class ArgumentParserTestCase(unittest.TestCase):
    def test_validates_input_path(self):
        with self.assertRaises(argparse.ArgumentTypeError) as e:
            self.parser = ArgParser("./wrong_path", "totalsize")

        self.assertEqual("Please enter valid path!", str(e.exception))

    def test_totalsize_does_not_accept_optional_arguments(self):
        with self.assertRaises(argparse.ArgumentTypeError) as e:
            self.parser = ArgParser("./tests/data", "totalsize", "--ext", "png")

        self.assertEqual(
            "Totalsize does not accept optional arguments!", str(e.exception)
        )


class CommandTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.tool_map = {"totalsize": TotalSize, "filter": Filter}

    def test_total_size_returns_total_size(self):
        parser = ArgParser("./tests/data", "totalsize")
        tool = self.tool_map[parser.arguments.tool](
            parser.arguments, parser.get_filter_functions()
        )
        files = [file for file in Path("./tests/data").rglob("*") if file.is_file()]

        EXPECTED_TOTAL_SIZE = (
            reduce(lambda prev, cur: prev + cur.stat().st_size, files, 0) / MB
        )

        total_size = tool.run()
        self.assertEqual(total_size, EXPECTED_TOTAL_SIZE)

    def test_filter_by_extension_returns_list_of_files(self):
        parser = ArgParser("./tests/data", "filter", "--ext", "pdf")
        tool = self.tool_map[parser.arguments.tool](
            parser.arguments, parser.get_filter_functions()
        )
        wd = os.getcwd()

        EXPECTED_FILE_LIST = sorted(
            [
                f"{wd}/tests/data/dir_1/dir_1_1/file2.pdf",
                f"{wd}/tests/data/dir_2/dir_2_1/file5.pdf",
                f"{wd}/tests/data/dir_2/dir_2_2/file5.pdf",
            ]
        )

        file_list = sorted(tool.run())
        self.assertEqual(file_list, EXPECTED_FILE_LIST)

    def test_filter_by_size_returns_list_of_files(self):
        parser = ArgParser("./tests/data", "filter", "--size", "1")
        tool = self.tool_map[parser.arguments.tool](
            parser.arguments, parser.get_filter_functions()
        )

        with NamedTemporaryFile(dir="./tests/data/dir_3", suffix=".txt") as file:
            file.write(b"\0" * 1024 * 1024 * 2)

            EXPECTED_FILE_LIST = [file.name]

            file_list = sorted(tool.run())
            self.assertEqual(file_list, EXPECTED_FILE_LIST)
