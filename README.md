# Folder search

Command line program for recursive file search and file size calculation.

It has two tools used for that purpose:

-   filter
-   totalsize

## Filter

Finds all files in specified directory recursively and filters them using one of following
options:

-   `--ext EXT` where `EXT` is file extension which determines file types to be returned
-   `--size SIZE` where `SIZE` is file size in MegaBytes used for filtering files -
    files larger than `SIZE` will be returned

### Example

```bash
python file.py /path/to/directory filter --ext txt
```

Will list all files with `.txt` extension in `/path/to/directory` and all of its subdirectories.

```bash
python file.py /path/to/directory filter --size 5
```

Will list all files larger than `5 MB` in `/path/to/directory` and all of its subdirectories.

## Total Size

Finds all files in specified directory recursively and returns cumulative file size in `MB`

### Example

```bash
python file.py /path/to/directory totalsize
```

Will return cumulative file size of all files in `/path/to/directory` and all of its subdirectories.
