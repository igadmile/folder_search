import argparse
import os
import typing as tp
from abc import ABC, abstractmethod
from functools import reduce
from pprint import PrettyPrinter

MB = 1024 * 1024

FilterFn = tp.Callable[[str], bool]


def check_has_file_type(file_type: str) -> FilterFn:
    def _check_has_file_type(file_path: str) -> bool:
        return file_path.split(".")[-1] == file_type

    return _check_has_file_type


def check_if_larger_than(size: int) -> FilterFn:
    def _check_if_larger_than(file_path: str) -> bool:
        return os.path.getsize(file_path) / MB > size

    return _check_if_larger_than


def sum_file_size(files: tp.Iterable[str]) -> float:
    return reduce(lambda prev, cur: prev + os.path.getsize(cur), files, 0) / MB


class Tool(ABC):
    def __init__(
        self,
        arguments: argparse.Namespace,
        filter_funcs: list[FilterFn],
    ):
        self.arguments = arguments
        self.filter_funcs = filter_funcs

    def get_files(self, path: str) -> tp.Generator[str, str, None]:
        wd = os.path.abspath(path)
        dir_elements = os.listdir(wd)
        subdirs: list[str] = []

        for dir_element in dir_elements:
            full_path = os.path.join(wd, dir_element)

            if os.path.isdir(full_path):
                subdirs.append(full_path)
            elif os.path.isfile(full_path):
                if all([f(full_path) for f in self.filter_funcs]):
                    yield full_path

        for subdir in subdirs:
            yield from self.get_files(subdir)

    @abstractmethod
    def run(self) -> tp.Union[list[str], float]:
        ...  # pragma: no cover


class Filter(Tool):
    def run(self):
        return list(self.get_files(self.arguments.dir_path))


class TotalSize(Tool):
    def run(self):
        return sum_file_size(self.get_files(self.arguments.dir_path))


class ArgParser:
    def __init__(self, *args):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument("dir_path")
        self.parser.add_argument("tool", choices=["filter", "totalsize"])
        self.parser.add_argument("--ext", help="file extension", type=str)
        self.parser.add_argument("--size", help="file size in MB", type=int)

        self.arguments = (
            self.parser.parse_args(args) if args else self.parser.parse_args()
        )
        self._validate_arguments()

    def _validate_arguments(self):
        if not os.path.isdir(self.arguments.dir_path):
            raise argparse.ArgumentTypeError("Please enter valid path!")

        if self.arguments.tool == "totalsize" and (
            self.arguments.size or self.arguments.ext
        ):
            raise argparse.ArgumentTypeError(
                "Totalsize does not accept optional arguments!"
            )

    def get_filter_functions(self) -> list[FilterFn]:
        filters = ("ext", "size")
        filter_map = {"ext": check_has_file_type, "size": check_if_larger_than}
        return [
            filter_map[f](getattr(self.arguments, f))  # type: ignore
            for f in filters
            if getattr(self.arguments, f) is not None
        ]


if __name__ == "__main__":  # pragma: no cover
    parser = ArgParser()
    filter_funcs = parser.get_filter_functions()
    tool_map = {"totalsize": TotalSize, "filter": Filter}
    tool = tool_map[parser.arguments.tool](
        parser.arguments, parser.get_filter_functions()
    )
    pp = PrettyPrinter()
    pp.pprint(tool.run())
