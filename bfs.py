import os
import typing as tp


def get_files_bfs(root: str):
    dirs = [root]

    while dirs:
        subdirs: list[str] = []

        for dir in dirs:
            for dir_element in os.listdir(dir):
                full_path = os.path.join(dir, dir_element)
                if os.path.isdir(full_path):
                    subdirs.append(full_path)
                elif os.path.isfile(full_path):
                    yield full_path

        dirs = subdirs


def get_files(path: str) -> tp.Generator[str, str, None]:
    wd: str = os.path.abspath(path)
    dir_elements = os.listdir(wd)
    subdirs: list[str] = []

    for dir_element in dir_elements:
        full_path = os.path.join(wd, dir_element)

        if os.path.isdir(full_path):
            subdirs.append(full_path)
        elif os.path.isfile(full_path):
            yield full_path

    for subdir in subdirs:
        yield from get_files(subdir)


def get_files_non_recursive(path: str):
    dirs = [path]

    while dirs:
        dir: str = dirs.pop()
        for dir_element in os.listdir(dir):
            full_path = os.path.join(os.path.abspath(dir), dir_element)
            if os.path.isdir(full_path):
                dirs.append(full_path)
            elif os.path.isfile(full_path):
                yield full_path


import time

time.sleep(20)
start_time = time.time()
print(len(list(get_files_bfs("/home/mile"))))
print("--- %s seconds ---" % (time.time() - start_time))

start_time = time.time()
print(len(list(get_files("/home/mile"))))
print("--- %s seconds ---" % (time.time() - start_time))

time.sleep(20)
start_time = time.time()
print(len(list(get_files_non_recursive("/home/mile"))))
print("--- %s seconds ---" % (time.time() - start_time))
